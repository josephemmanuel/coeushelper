package com.coeusApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoeusAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoeusAppApplication.class, args);
	}

}

