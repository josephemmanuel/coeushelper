package com.coeusApp;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CoeusRepository extends  CrudRepository<Coeus, Long>{

	List<Coeus> findByUserNameAndParameter(String module, String parameters);
}
