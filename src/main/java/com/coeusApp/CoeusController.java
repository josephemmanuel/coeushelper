package com.coeusApp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class CoeusController {
	
	@Autowired
	CoeusRepository repository;
	
	@GetMapping("/search")
	public List<Coeus> search(@RequestParam("module") String module, @RequestParam("parameters") String parameters) {
		return repository.findByUserNameAndParameter(module,parameters);
	}
	
	
}
